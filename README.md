# Particle of Feline

A small first-person dungeon crawler with mild NSFW elements, made in Castle Game Engine.

## Installation

### Windows

No installation required - just extract the game into one folder and play.

Known bug on Windows 10: Avoid putting the game into a folder with non-ASCII characters if your user name also contains non-ASCII characters.

### Linux

No installation required - just extract the game into one folder and play.

Sometimes you might also need to set "executable" flag for the binary (`chmod +x catoban` or through file properties in the file manager).

You need the following libraries installed to play the game:

* libopenal1
* libpng
* zlib1g
* libvorbis
* libfreetype6
* You also need X-system, GTK at least version 2 and OpenGL drivers for your videocard, however, you are very likely to have those already installed :)

Alternatively on Debian-based systems like Ubuntu you can download the DEB package and install it through a package manager or by `dpkg -i catoban-*-linux-x86_64.deb` command.

### Android

Download and install the APK. The game requires Android 4.2 and higher.

You may need to allow installation of third-party APKs in the phone/tablet Settings.

## Credits

Game by EugeneLoza

Created in Castle Game Engine (https://castle-engine.io/)

TODO************************************

Fonts: Angel Koziupa, Alejandro Paul

## Links

Download the latest version: TODO************************************** [Windows, Linux, Android]

Source code: https://gitlab.com/EugeneLoza/particle-of-feline (GPLv3)
