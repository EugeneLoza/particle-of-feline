{ Copyright (C) 2022-2022 Yevhen Loza

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program. If not, see <http://www.gnu.org/licenses/>. }

unit GameInitialize;

{$INCLUDE compilerconfig.inc}

interface

implementation

uses SysUtils,
  CastleWindow, CastleLog, CastleUIState, CastleConfig, CastleSoundEngine,
  CastleApplicationProperties, CastleKeysMouse, CastleUiControls, CastleColors
  {$region 'Castle Initialization Uses'}
  // The content here may be automatically updated by CGE editor.
  , GameStateMain
  , GameStateGame
  {$endregion 'Castle Initialization Uses'},
  GameSounds;

var
  Window: TCastleWindow;

{$IFNDEF Mobile}
procedure WindowPress(Container: TUIContainer; const Event: TInputPressRelease);
begin
  if Event.IsKey(keyF11) then
  begin
    Window.FullScreen := not Window.FullScreen;
    UserConfig.SetValue('fullscreen', Window.FullScreen);
  end;

  if Event.IsKey(keyF5) then
  begin
    Container.SaveScreen('screenshot_vinculike_' + IntToStr(Round(Now * 24 * 60 * 60 * 100)) + '.png');
    Sound('screenshot');
  end;
end;
{$ENDIF}

procedure WindowClose(Container: TUIContainer);
begin
  {$IFNDEF Mobile} // We should not save the game on mobile
  //SaveGame;
  {$ENDIF}
  Window.Close;
end;

procedure ApplySettings;
begin
  Window.FullScreen := UserConfig.GetValue('fullscreen', true);
  SoundEngine.Volume := UserConfig.GetFloat('master_volume', DefaultVolume);
  SoundEngine.LoopingChannel[0].Volume := UserConfig.GetFloat('music_volume', 1.0);
  ApplicationProperties.LimitFPS := UserConfig.GetInteger('limit_fps', 60);
end;

{ One-time initialization of resources. }
procedure ApplicationInitialize;
begin
  Window.Container.LoadSettings('castle-data:/CastleSettings.xml');
  UserConfig.Load;
  SoundEngine.RepositoryURL := 'castle-data:/audio/index.xml';
  ApplySettings;
  InitializeSounds;

  {$region 'Castle State Creation'}
  // The content here may be automatically updated by CGE editor.
  StateMain := TStateMain.Create(Application);
  StateGame := TStateGame.Create(Application);
  {$endregion 'Castle State Creation'}

  {if UserConfig.GetValue('ask_content_warning', true) or UserConfig.GetValue('always_ask_content_warning', false) then
    TUIState.Current := StateContentWarning
  else}
    TUIState.Current := StateMain;
end;

initialization
  WriteLnLog('----------------------------------------------------');
  WriteLnLog(ApplicationProperties.Caption + ' ' + ApplicationProperties.Version);
  WriteLnLog('Copyright (C) 2022-2022 Yevhen Loza');
  WriteLnLog('This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.');
  WriteLnLog('This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.');
  WriteLnLog('You should have received a copy of the GNU General Public License along with this program. If not, see <http://www.gnu.org/licenses/>.');
  WriteLnLog('----------------------------------------------------');

  Application.OnInitialize := @ApplicationInitialize;

  Theme.LoadingBackgroundColor := HexToColor('13120e');
  //Theme.ImagesPersistent[tiLoading].Image := Splash;
  Theme.ImagesPersistent[tiLoading].OwnsImage := false;
  Theme.ImagesPersistent[tiLoading].SmoothScaling := true;
  Theme.LoadingUIScaling := usEncloseReferenceSize;
  Theme.LoadingUIReferenceWidth := 1920;
  Theme.LoadingUIReferenceHeight := 1080;

  Window := TCastleWindow.Create(Application);
  Window.FpsShowOnCaption := true;
  Window.ParseParameters; // allows to control window size / fullscreen on the command-line

  {$IFNDEF Mobile}
    Window.Height := Application.ScreenHeight * 4 div 5;
    Window.Width := Window.Height * 1334 div 750;
    Window.OnPress := @WindowPress;
  {$ENDIF}
  //Window.AlphaBits := 8;
  Window.OnCloseQuery := @WindowClose;
  Window.Container.BackgroundEnable := false;

  Application.MainWindow := Window;
end.
