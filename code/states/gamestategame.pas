{ Copyright (C) 2022-2022 Yevhen Loza

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program. If not, see <http://www.gnu.org/licenses/>. }

unit GameStateGame;

interface

uses Classes,
  CastleVectors, CastleUIState, CastleComponentSerialize,
  CastleUIControls, CastleControls, CastleKeysMouse;

type
  TStateGame = class(TUIState)
  private
    //LabelFps: TCastleLabel;
  public
    constructor Create(AOwner: TComponent); override;
    procedure Start; override;
    procedure Update(const SecondsPassed: Single; var HandleInput: Boolean); override;
    function Press(const Event: TInputPressRelease): Boolean; override;
  end;

var
  StateGame: TStateGame;

implementation

uses
  SysUtils,
  CastleSoundEngine;

constructor TStateGame.Create(AOwner: TComponent);
begin
  inherited;
  DesignPreload := true;
  DesignUrl := 'castle-data:/gamestategame.castle-user-interface';
end;

procedure TStateGame.Start;
begin
  inherited;
  SoundEngine.LoopingChannel[0].Sound := SoundEngine.SoundFromName('music_game');

  //LabelFps := DesignedComponent('LabelFps') as TCastleLabel;
end;

procedure TStateGame.Update(const SecondsPassed: Single; var HandleInput: Boolean);
begin
  inherited;
  //LabelFps.Caption := 'FPS: ' + Container.Fps.ToString;
end;

function TStateGame.Press(const Event: TInputPressRelease): Boolean;
begin
  Result := inherited;
  if Result then Exit; // allow the ancestor to handle keys
end;

end.
