{ Copyright (C) 2022-2022 Yevhen Loza

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program. If not, see <http://www.gnu.org/licenses/>. }

unit GameStateMain;

interface

uses Classes,
  CastleVectors, CastleUIState, CastleComponentSerialize,
  CastleUIControls, CastleControls, CastleKeysMouse;

type
  TStateMain = class(TUIState)
  private
    procedure ClickNewGame(Sender: TObject);
    //LabelFps: TCastleLabel;
  public
    constructor Create(AOwner: TComponent); override;
    procedure Start; override;
  end;

var
  StateMain: TStateMain;

implementation

uses
  SysUtils,
  CastleSoundEngine,
  GameStateGame;

procedure TStateMain.ClickNewGame(Sender: TObject);
begin
  SoundEngine.Play(SoundEngine.SoundFromName('button'));
  TUiState.Current := StateGame;
end;

constructor TStateMain.Create(AOwner: TComponent);
begin
  inherited;
  DesignUrl := 'castle-data:/gamestatemain.castle-user-interface';
end;

procedure TStateMain.Start;
begin
  inherited;
  SoundEngine.LoopingChannel[0].Sound := SoundEngine.SoundFromName('music_menu');
  (DesignedComponent('ButtonNewGame') as TCastleButton).OnClick  := @ClickNewGame;
end;

end.
