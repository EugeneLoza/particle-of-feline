Felina stood before a massive iron door. Why was she taking her exam in the College of Magic's vivarium? Wouldn't an arena be a more appropriate place for a Gladiator University graduation? But what worried her most was the fact that she wasn't allowed to carry her sword with. What does it even have to do with... anything, if she can't slice things right and left.

Mrs. Squirrly finished fixing the emergency escape charm on Felina's neck. Safety precautions... yeah. Why adults are so boring? Felina caught herself at the thought she is legally an adult for several years already and it's only to her parents, Cheety and Jag, she is a "little fur-baby".

"Good to go" Squirrly said. "So, as I've already said. You go in, go out. 20 minutes adventure. Grab the Particle of Life, bring it back. And don't look at me like that. I have no idea why the Archmage wanted this challenge for your finals. Ready?"

The Argchmage? What does Ms. Lizzy have to do with that? Felina was smart enough not to ask such questions and simply nodded.

She would be much more self-confident with a katzbalger and a cheering crowd, not damp dungeon walls. But not much of a choice, eh? Exams are only a formality. What could possibly go wrong?

============== repo

It's not her first visit to the Vivarium, but somehow she has never seen anything like that around here.

You will know it when you see it.
